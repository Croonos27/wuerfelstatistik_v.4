const buttonWuerfeln = document.querySelector('#buttonWuerfeln');
const td2er = document.querySelector('#td2er');
const td3er = document.querySelector('#td3er');
const td4er = document.querySelector('#td4er');
const td5er = document.querySelector('#td5er');
const td6er = document.querySelector('#td6er');
const td7er = document.querySelector('#td7er');
const td8er = document.querySelector('#td8er');
const td9er = document.querySelector('#td9er');
const td10er = document.querySelector('#td10er');
const td11er = document.querySelector('#td11er');
const td12er = document.querySelector('#td12er');
const durchschnitt = document.querySelector('#durchschnitt');
const randomWuerfe = document.querySelector('#randomWuerfe');
let speicher1 = 0;
let speicher2 = 0;
let durchschnitt1 = 0;
let anzahlWuerfe = 0;



//-------------------------------------------
// 2. Daten (Model)
//-------------------------------------------
const anzahl = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];



//-------------------------------------------
// 3. Funktionsdefinitionen
//-------------------------------------------
// a) Model: Businesslogik
function allesAufNull() {
    for (let i = 0; i < anzahl.length; i++) {
        anzahl[i] = 0;
    }
    durchschnitt1 = 0;
}
function anzahlWuerfeRandom() {
    anzahlWuerfe = 0;
    anzahlWuerfe = (Math.floor(Math.random() * 6) + 1) * 1000000;
}

function wuerfeln() {

    for (let i = 0; i < anzahlWuerfe; i++) {
        const ergebnis1 = Math.floor(Math.random() * 6) + 1;
        const ergebnis2 = Math.floor(Math.random() * 6) + 1;

        //anzahl[ergebnis1-1] = anzahl[ergebnis1-1] +1;
        speicher1 = ergebnis1;
        speicher2 = ergebnis2;
        anzahl[speicher1 + speicher2] += 1;
        durchschnitt1 += speicher1 + speicher2;
    }
}


// b) View: Darstellung
function ausgeben() {
    td2er.innerText = anzahl[2];
    td3er.innerText = anzahl[3];
    td4er.innerText = anzahl[4];
    td5er.innerText = anzahl[5];
    td6er.innerText = anzahl[6];
    td7er.innerText = anzahl[7];
    td8er.innerText = anzahl[8];
    td9er.innerText = anzahl[9];
    td10er.innerText = anzahl[10];
    td11er.innerText = anzahl[11];
    td12er.innerText = anzahl[12];
    durchschnitt.innerText = "Durchschnitt: "+durchschnitt1 / anzahlWuerfe
    randomWuerfe.innerText = "AnzahlWürfe: "+anzahlWuerfe;
    console.log(anzahlWuerfe);
}
// c) Event-Listener
function buttonWuerfelnClick() {
    allesAufNull();
    anzahlWuerfeRandom();
    wuerfeln();
    ausgeben();
}

function start() {
    allesAufNull();
}

//-------------------------------------------
// 4. Event-Listener registrieren
//-------------------------------------------
buttonWuerfeln.addEventListener('click', buttonWuerfelnClick);


//-------------------------------------------
// 5. Hauptprogramm
//-------------------------------------------
start();